import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(

        brightness: Brightness.light,
        listTileTheme: ListTileThemeData(iconColor: Colors.blue),
        //iconTheme: IconThemeData(color: Colors.black12),
       appBarTheme: const AppBarTheme(
            foregroundColor: Colors.white,
            color: Colors.blue, iconTheme: IconThemeData(color: Colors.white)),
        iconTheme: IconThemeData(color: Colors.lightBlue),

    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
        brightness: Brightness.dark,
        listTileTheme: ListTileThemeData(iconColor: Colors.red),
        //iconTheme: IconThemeData(color: Colors.black),
        appBarTheme: AppBarTheme(
            foregroundColor: Colors.red,
            color: Colors.black12, iconTheme: IconThemeData(color: Colors.red)),
        iconTheme: IconThemeData(color: Colors.red),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
        foregroundColor: Colors.black,
            backgroundColor: Colors.red
      )
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  bool click = true;
  var currentTheme = APP_THEME.DARK;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              setState(() {
                click = !click;
                currentTheme == APP_THEME.DARK
                    ? currentTheme = APP_THEME.LIGHT
                    : currentTheme = APP_THEME.DARK;
              });
            },
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child:
                  Icon((click == false) ? Icons.nightlight_round : Icons.sunny),
            )

            // Icon(Icons.threesixty),

            // onPressed: () { setState(() {
            //     currentTheme == APP_THEME.DARK? currentTheme = APP_THEME.LIGHT : currentTheme = APP_THEME.DARK;
            //   });
            //   },
            ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.grey,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          //color: Colors.grey,
        ),
        onPressed: () {},
      ),
      Text("Text")
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.grey,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.mail,
          // color: Colors.grey,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          //color: Colors.grey,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //color: Colors.grey,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneTileList() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("093-817-6074"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.mail),
    title: Text("nontapat56@gmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text(" 42/2, Bangsaen, Chonburi"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      onPressed: () {},
    ),
  );
}

PreferredSizeWidget buildAppBarWidget() {
  return AppBar(
    title: const Text("Profile"),
    leading: const Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: const Icon(Icons.star_border),
      )
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
//Image.network('https://uploads.dailydot.com/2018/10/olli-the-polite-cat.jpg?q=65&w=800&ar=2:1&fit=crop'),
          Container(
              color: Colors.blue,
              width: double.infinity,

//Height constraint at Container widget level
              height: 250,
              child: Image.network(
                'https://scontent.fbkk10-1.fna.fbcdn.net/v/t31.18172-8/16836152_1864156580540869_2523216214694487667_o.jpg?_nc_cat=101&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeGyFXT3elGpuPk_HB7lsT7UkK0El5fZYnCQrQSXl9licBhMNjeU9NLrzFDoPQamA6ZC2limgb4KVsT122R2l9wC&_nc_ohc=EeCgWOhz1aYAX-fCj6I&_nc_ht=scontent.fbkk10-1.fna&oh=00_AfARHo6MWT5M7BotSYTYmn2O_U4fnFm0aEzetNm9ukkLHA&oe=63CA074D',
                fit: BoxFit.cover,
              )),
          Container(
            padding: const EdgeInsets.all(10),
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: const <Widget>[
                Text(
                  "Nontapat Wisesvongsa",
                  style: TextStyle(fontSize: 34),
                ),
              ],
            ),
          ),
          const Divider(
            thickness: 2.0,
          ),
          Container(
            margin: const EdgeInsets.only(top: 8, bottom: 8),
            child: profileActionItems(),
          ),
          const Divider(
            thickness: 2.0,
          ),
          mobilePhoneTileList(),
          emailListTile(),
          addressListTile()
        ],
      )
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionButton(),
      buildPayButton()
    ],
  );
}
